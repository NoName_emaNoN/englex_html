/*$.get('example_collection.json', function(data){
 $("#name").typeahead({ source:data });
 },'json');*/
//example_collection.json
// ["item1","item2","item3"]

$(function () {
	var source = [
		{"id": "1", "name": "Александр Стручковский", "type": "Студент", "icon": "smile-o"},
		{"id": "2", "name": "Александр Скворцовский", "type": "Студент", "icon": "smile-o"},
		{"id": "3", "name": "Александра Печкина", "type": "Студент", "icon": "smile-o"},
		{"id": "4", "name": "Александр Стручковский", "type": "Студент", "icon": "smile-o"},
		{"id": "5", "name": "Александра Семилет", "type": "Преподаватель", "icon": "briefcase"},
		{"id": "6", "name": "Александра Сыровец", "type": "Преподаватель", "icon": "briefcase"},
	];


	$('#search').typeahead({
		source: function (query, process) {
			states = [];
			map = {};

			$.each(source, function (i, state) {
				map[state.name] = state;
				states.push(state.name);
			});

			process(states);
		},
		updater: function (item) {
			selectedState = map[item].id;
			return item;
		},
		matcher: function (item) {
			if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
				return true;
			}

			return false;
		},
		sorter: function (items) {
			return items.sort();
		},
		highlighter: function (item) {
			var regex = new RegExp('(' + this.query + ')', 'gi');
			return '<div class="clearfix"> <div class="pull-left">' + item.replace(regex, "<strong>$1</strong>") + '<div class="type">' + map[item].type + '</div> </div><div class="pull-right"><i class="fa fa-' + map[item].icon + '"></i></div></div>';
		}
	});

	$('.datepicker-input').datepicker({language: "ru"});

	$('.show-password-link').on('click', function () {
		var i = $(this).prev('input');

		if (i.attr('type') == 'password') {
			i.attr('type', 'text');
		} else {
			i.attr('type', 'password');
		}

		return false;
	});

	$(document).on('click', '.user-info-collapse a', function () {
		$(this).parent().hide();

		$(this).parent().next('ul').fadeIn('fast');

		$(this).closest('table').find('.hidden').removeClass('hidden');
		$(this).closest('table').find('.last-row').removeClass('last-row');

		return false;
	});

	$(document).on('click', '.working-hours-checkbox', function () {
		if ($(this).hasClass('off')) {
			$(this).removeClass('off').addClass('on').find('i').removeClass('fa-ban').addClass('fa-check-circle');
		} else {
			$(this).removeClass('on').addClass('off').find('i').removeClass('fa-check-circle').addClass('fa-ban');
		}

		return false;
	});

	$('.working-hours-checkbox').hover(function () {
		var index = $(this).parent().index();

		/* Remove hover class */
		$(this).parents('table').find('td,th').removeClass('hover');

		/* Add hover class by horizontal */
		$(this).parent().prevAll().addClass('hover');

		/* Add hover class by vertical */
		$(this).parents('tr').prevAll('tr').find('td:eq(' + (index - 1) + ')').addClass('hover');
		$(this).parents('table').find('thead>tr>th:eq(' + index + ')').addClass('hover');
	});

	$('.working-hours-table>tbody').mouseleave(function () {
		/* Remove hover class */
		$(this).parents('table').find('td,th').removeClass('hover');
	});
});